var exec = require('child_process').exec;
var createClosurePreprocessor = function(args, config, logger) {
    config = config || {};

    var log = logger.create('preprocessor.closure');

    return function(content, file, done) {
        var command = config.depsWriter +
        ' --root_with_prefix="' + config.root + ' ' + config.rootPrefix + '"' +
        ' --output_file="' + config.outputFile + '"';
        log.info(command);
        exec(command, function(error, stdout, stderr) {
            console.log('stdout: ' + stdout);
            console.log('stderr: ' + stderr);
            done(error, content);
        });
    };
};

createClosurePreprocessor.$inject = ['args', 'config.closurePreprocessor', 'logger'];

// PUBLISH DI MODULE
module.exports = {
    'preprocessor:closure': ['factory', createClosurePreprocessor]
};