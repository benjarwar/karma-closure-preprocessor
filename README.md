# karma-closure-preprocessor

> Preprocessor to watch src js files changes and update closure dependency on the fly.

## Installation

The easiest way is to keep `karma-closure-preprocessor` as a devDependency in your `package.json`.
```json
{
  "devDependencies": {
    "karma": "~0.10",
    "karma-closure-preprocessor": "~0.0.7"
  }
}
```

You can simple do it by:
```bash
npm install karma-closure-preprocessor --save-dev
```

## Configuration
Following code shows the example configuration...
```js
// karma.conf.js
module.exports = function(config) {
  config.set({
    files: [
      {
          pattern: 'bower_components/closurelibrary/closure/goog/base.js',
          watched: false,
          included: true,
          served: true
      },
      {
          pattern: 'bower_components/closurelibrary/closure/**/*.js',
          watched: false,
          included: false,
          served: true
      },
      {
          pattern: 'src/**/*.js',
          watched: true,
          included: false,
          served: true
      },
      'test/unit-deps.js',
      'test/unit/**/*.js'
    ],
    preprocessors: {
      'src/**/*.js': ['closure']
    },
    closurePreprocessor: {
      depsWriter: __dirname + '/bower_components/closurelibrary/closure/bin/build/depswriter.py',
      root: __dirname + '/src/',
      rootPrefix: '../../../../src/',
      outputFile: __dirname + '/test/unit-deps.js'
    }
  });
};
```

----

For more information on Karma see the [homepage].


[homepage]: http://karma-runner.github.com